package com.baglie.enemyservice.service;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.dto.UnitDto;
import com.baglie.enemyservice.entity.Enemy;
import com.baglie.enemyservice.entity.Unit;
import com.baglie.enemyservice.repository.BattleManagerRepository;
import com.baglie.enemyservice.repository.EnemyRepository;
import lombok.RequiredArgsConstructor;
import com.baglie.enemyservice.exceptions.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EnemyService {

    private final EnemyRepository enemyRepository;
    private final BattleManagerRepository battleManagerRepository;

    public Unit createEnemy(Unit enemy) {
        return enemyRepository.save(enemy);
    }

    public Unit findByPosition(Integer posX, Integer posY){
        return enemyRepository.findByPosXAndPosY(posX, posY).orElseThrow(NotFoundException::new);
    }

    public void registerEnemyOnBattlefield() {

        List<Unit> enemies = enemyRepository.findAll();

        enemies.forEach(enemy -> {
            UnitDto unitDto = new UnitDto();

            unitDto.setPosX(enemy.getPosX());
            unitDto.setPosY(enemy.getPosY());
            unitDto.setProtectionLevel(enemy.getProtectionLevel());
            unitDto.setUnitType(enemy.getUnitType());
            unitDto.setIsAlive(enemy.getIsAlive());

            battleManagerRepository.registerEnemyOnBattlefield(unitDto);
        });
    }

    public void updatePosition(PositionUpdateDto positionUpdateDto){
        Unit enemy = findByPosition(positionUpdateDto.getPosX(), positionUpdateDto.getPosY());

        enemy.setPosY(positionUpdateDto.getNewPosY());
        enemyRepository.save(enemy);
        battleManagerRepository.updateUnitPosition(positionUpdateDto);
    }

    public void startFight() {
        List<Unit> enemies =  enemyRepository.findAll();

        enemies.forEach(enemy -> {
            Unit unit = new Unit(
                    enemy.getPosX(),
                    enemy.getPosY(),
                    enemy.getProtectionLevel(),
                    enemy.getUnitType(),
                    enemy.getIsAlive(),
                    enemy.getSpeed()
            );
            new Thread((Enemy) unit).start();
        });
    }

}
