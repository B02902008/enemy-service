package com.baglie.enemyservice.repository;

import com.baglie.enemyservice.entity.Enemy;
import com.baglie.enemyservice.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EnemyRepository extends JpaRepository<Unit, Integer> {

    Optional<Unit> findByPosXAndPosY(Integer posX, Integer posY);
}
