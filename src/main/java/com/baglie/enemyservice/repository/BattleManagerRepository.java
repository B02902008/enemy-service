package com.baglie.enemyservice.repository;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.dto.UnitDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
@RequiredArgsConstructor
public class BattleManagerRepository {

    private final RestTemplate restTemplate = new RestTemplate();

    public void registerEnemyOnBattlefield(UnitDto unitDto) {

        String url = "http://localhost:8082/units/";
        HttpEntity<UnitDto> request = new HttpEntity<>(unitDto);

        restTemplate.postForObject(url, request, Void.class);
    }

    public void updateUnitPosition(PositionUpdateDto updateDto) {

        String url = "http://localhost:8082/units//position/update";
        HttpEntity<PositionUpdateDto> request = new HttpEntity<>(updateDto);

        restTemplate.patchForObject(url, request, Void.class);
    }

}
