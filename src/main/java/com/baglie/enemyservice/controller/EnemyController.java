package com.baglie.enemyservice.controller;

import com.baglie.enemyservice.entity.Enemy;
import com.baglie.enemyservice.entity.Unit;
import com.baglie.enemyservice.service.EnemyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;


@RestController
@RequestMapping("/enemies")
@RequiredArgsConstructor
public class EnemyController {

    private final EnemyService enemyService;

    @PostMapping
    public ResponseEntity<Unit> createEnemy(@RequestBody Unit enemy) {
        enemyService.createEnemy(enemy);
        return ResponseEntity
                .created(URI.create("enemy/"+ enemy.getId()))
                .build();
    }

    @PostMapping("/register")
    public ResponseEntity<?> register() {
        enemyService.registerEnemyOnBattlefield();
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/fight")
    public ResponseEntity<?> startFight() {
        enemyService.startFight();
        return ResponseEntity.noContent().build();
    }

}
