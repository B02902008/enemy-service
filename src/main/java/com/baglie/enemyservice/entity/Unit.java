package com.baglie.enemyservice.entity;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.service.EnemyService;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;


import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
public class Unit {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private Integer posX;
    private Integer posY;
    private Integer protectionLevel;
    private Integer speed;

    @Enumerated(EnumType.STRING)
    private UnitType unitType;

    private Boolean isAlive;

    public Unit(){}

    public Unit(Integer posX, Integer posY, Integer protectionLevel, UnitType unitType, Boolean isAlive, Integer speed) {
        this.posX = posX;
        this.posY = posY;
        this.protectionLevel = protectionLevel;
        this.unitType = unitType;
        this.isAlive = isAlive;
        this.speed = speed;
    }

}
