package com.baglie.enemyservice.entity;

public enum UnitType {
    AFC,
    TANK,
    INFANTRY,
    UNKNOWN
}