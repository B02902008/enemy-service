package com.baglie.enemyservice.entity;

import com.baglie.enemyservice.dto.PositionUpdateDto;
import com.baglie.enemyservice.service.EnemyService;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.TimeUnit;


@RequiredArgsConstructor
public class Enemy extends Unit implements Runnable {

    private final EnemyService enemyService;
    private final PositionUpdateDto updateDto;

    protected void move() {

        while (getIsAlive()) {

            updateDto.setPosX(getPosX());
            updateDto.setPosY(getPosY());
            updateDto.setNewPosX(getPosX());
            updateDto.setNewPosY(getPosY() - 1);

            enemyService.updatePosition(updateDto);

            try {
                TimeUnit.SECONDS.sleep(getSpeed());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        move();
    }
}
